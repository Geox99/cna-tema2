﻿using Grpc.Core;
using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Tema2CNA.Protos;

namespace Tema2CNA.Services
{
    public class ZodiacService : Zodiac.ZodiacBase
    {
        public override Task<ZodiacReply> GetZodiacSign(DateRequest request, ServerCallContext context)
        {
            var birthday = request.Birthday;
            string[] split = birthday.Split('/');
            var year = int.Parse(split[2]);
            var month = int.Parse(split[0]);
            var day = int.Parse(split[1]);
            var path = "zodii.txt";
            var file = File.ReadAllLines(path);
            string sign = GetSign(file, year,month,day);

            Console.WriteLine("Your sign is : " + sign);
            return Task.FromResult(new ZodiacReply(){Sign = sign});
        }

        public static string GetSign(string[] file, int year, int month,int day)
        {
            DateTime firstDay;
            DateTime lastDay;
            string sign;
            string birth;

            for (int i = 0 ; i< file.Length; i+=3)
            {
                sign = file[i];
                DateTime dateTime1 = DateTime.Parse(file[i + 1]);
                firstDay = dateTime1.AddYears(-dateTime1.Year + year);
                DateTime dateTime2 = DateTime.Parse(file[i + 2]);
                lastDay = dateTime2.AddYears(-dateTime2.Year + year);
                if(month<10)
                    birth ="0"+month + "/" + day + "/" + year;
                else
                    birth = month + "/" + day + "/" + year;
                DateTime birthDate = DateTime.Parse(birth);
                if (firstDay <= birthDate && birthDate <= lastDay)
                    return sign;
            }

            return "Error";
        }
    }
}
